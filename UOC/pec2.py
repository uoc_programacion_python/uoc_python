
import random

# Generamos dos numeros aleatorios entre 0 y 100

a = random.randint(0, 100)
b = random.randint(0, 100)

print "Los numeros aleatorios generados han sido %d y %d" % (a, b)

c = a * b  # Almacenamos la multiplicacion de los dos numeros en otra variable c

print "El cuadrado de la multiplicacion de estos dos numeros es %d" % c ** 2

dias_semana = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo']

print dias_semana[0], dias_semana[-1]

import math  # Importo la biblioteca Math para poder obtener el valor de pi
r_ = 3
pi_ = math.pi  # Asigno el valor de PI a la variable pi_
area_ = pi_ * (r_ ** 2)  # Calculo del area
print "El area de un circulo de radio 3 cm es igual a %2.2f cm2"  % area_


a = 3*2

print a

b = 9/2.

print b

c, a = a+1, b*2

print c

print a, b, c

# Ejercicio 4: Crear un Diccionario

print 'Ejercicio 4'
prefijos_Espana = {91: 'Madrid', 93: 'Barcelona', 973: 'Lleida', 95: 'Melilla'}
print prefijos_Espana[91]

# Recuperar la clave de un valor
print prefijos_Espana.iteritems()

print prefijos_Espana.get('Madrid')


'''a = 5

b = 4

print a, b

b = a

print a, b

a = 6

print a, b
'''

a = 3*2

b = 9/2.

c, a = a+1, b*2

print a, b, c

a = 3*2

b = 9/2.

a, c = b*2, a+1

print a, b, c


planetas = ('Saturno', 'Jupiter' 'Tierra')

print planetas

t = '123', 'abc'
print t

print type(t)
print type(planetas)


jueves = ['J', 'u', 'e', 'v', 'e', 's']
print ''.join(jueves)

dias = ['Lunes', 'Martes', "Miercoles", ''.join(['J','u','e','v','e','s'])]
print dias

matriz = [[1],[2],[3]]
print matriz

x = [i for i in range(10) if i%2==0]
y = [i for i in range(10) if i not in x]
print x
print y





'''
* dias = ['Lunes', 'Martes', "Miercoles", ''.join(['J','u','e','v','e','s'])]
* matriz = [[],[],[]]
* t = '123','abc'
'''
